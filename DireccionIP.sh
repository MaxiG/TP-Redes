#!/bin/bash

ip=$(hostname -I)

red=$(route | grep -oiE '([0-9]{1,3}\.){3}[0-9]{1,3}' | grep -v 0.0.0.0|grep -v 255)

#-o para indicar que la salida del comando debe tener sólo lo que coincide con el patrón.
# -i ignora la distinción entre mayusculas y minusculas
#-E  indica que se va a utilizar una expresión larga.
#  ([0-9]{1,3}\.){3} para tres bloques de hasta tres numeros separados por puntos.
# [0-9]{1,3} es para el ultimo numero de la IP que tiene hasta tres digitos.
# -v para que no muestre la info pasada como parametro. 

echo "Mi Dirección de Red es: $red"

echo "Mi IP es $ip"
